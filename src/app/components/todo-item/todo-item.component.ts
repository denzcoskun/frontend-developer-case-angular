import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss']
})
export class TodoItemComponent implements OnInit{

    @Input() todoItem: any;
    @Input() user: any;
    @Output() changeData = new EventEmitter();

    constructor(){

    }

    ngOnInit(): void {
        console.log(this.todoItem);
    }

    onChangeData(type: boolean): void{
      this.changeData.emit(type);
    }

}
