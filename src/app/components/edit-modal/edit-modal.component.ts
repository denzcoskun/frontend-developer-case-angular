﻿import { Component, ViewEncapsulation, ElementRef, Input, OnInit, OnDestroy } from '@angular/core';

import { EditModalService } from './editmodal.service';

@Component({
    selector: 'app-edit-modal',
    templateUrl: 'edit-modal.component.html',
    styleUrls: ['edit-modal.component.less'],
    encapsulation: ViewEncapsulation.None
})
export class EditModalComponent implements OnInit, OnDestroy {
    @Input() id!: string;
    private element: any;

    constructor(private editModalService: EditModalService, private el: ElementRef) {
        this.element = el.nativeElement;
    }

    ngOnInit(): void {
        if (!this.id) {
            return;
        }

        document.body.appendChild(this.element);

        this.element.addEventListener('click', (el: any) => {
            if (el.target.className === 'app-edit-modal') {
                this.close();
            }
        });

        this.editModalService.add(this);
    }

    ngOnDestroy(): void {
        this.editModalService.remove(this.id);
        this.element.remove();
    }

    open(): void {
        this.element.style.display = 'block';
        document.body.classList.add('app-edit-modal-open');
    }

    close(): void {
        this.element.style.display = 'none';
        document.body.classList.remove('app-edit-modal-open');
    }
}
