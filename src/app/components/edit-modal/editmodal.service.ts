﻿import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class EditModalService {
    private editModals: any[] = [];

    add(modal: any): void{
        this.editModals.push(modal);
    }

    remove(id: string): void{
        this.editModals = this.editModals.filter(modal => modal.id !== id);
    }

    open(id: string): void{
        const editModal = this.editModals.find(modal => modal.id === id);
        editModal.open();
    }

    close(id: string): void{
        const editModal = this.editModals.find(modal => modal.id === id);
        editModal.close();
    }
}
