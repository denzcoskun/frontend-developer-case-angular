import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class TodolistService {

    constructor(private http: HttpClient) {}

    public getTodos(): Promise<any> {
        return this.http.get(environment.serverUrl + '/todos').toPromise();
    }

    public getUsers(): Promise<any> {
        return this.http.get(environment.serverUrl + '/users').toPromise();
    }

    public patchTodo(id: number, data: any): Promise<any> {
        return this.http.patch(environment.serverUrl + '/todos/' + id, data).toPromise();
    }

    public deleteTodo(id: number): Promise<any> {
        return this.http.delete(environment.serverUrl + '/todos/' + id).toPromise();
    }

}
