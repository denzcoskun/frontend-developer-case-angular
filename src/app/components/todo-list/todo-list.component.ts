import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { Todo } from '../../models/todo';
import { User } from '../../models/user';
import { EditModalService } from '../edit-modal/editmodal.service';
import { TodolistService } from './todo-list.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit{

    private todos: Todo[] = [];
    private users: User[] = [];

    shownList: Todo[] = [];
    pagerSize = 0;
    pagerNumber = 0;
    pagerList: number[] = [];
    ascending = true;

    @ViewChild('editText') editText!: ElementRef;
    @ViewChild('checkbox') checkbox!: ElementRef;
    editedTodo!: Todo;

    constructor(private todoListService: TodolistService,
                private editModalService: EditModalService){

    }

    ngOnInit(): void {
        forkJoin([this.todoListService.getTodos(), this.todoListService.getUsers()])
        .subscribe(([todos, users]) => {
            if (todos){
                this.todos = todos;
                this.getPagerSize();
            }

            if (users){
                this.users = users;
            }
        });
    }

    getUser(id: number): any{
        return this.users.find((user: any) => user.id === id);
    }

    getPage(index: number): void{
        this.shownList = this.todos.slice(index * 10, (index + 1) * 10);
        if (this.shownList.length === 0){
            this.pagerNumber = this.pagerNumber - 1;
            this.getPage(index - 1);
        }
    }

    getPagerSize(): void{
        this.pagerList = [];
        this.pagerSize = Math.ceil(this.todos.length / 10);
        for (let i = 0; i < this.pagerSize; i++) {
            this.pagerList.push(i + 1);
        }
        this.getPage(this.pagerNumber);
    }

    onSelectPage(index: number): void{
        this.pagerNumber = index;
        this.getPage(index);
    }

    onChangePage(target: number): void{
        this.pagerNumber = this.pagerNumber + target;
        this.getPage(this.pagerNumber);
    }

    onStatusSort(): void {
        this.ascending = !this.ascending;
        this.ascending ?
             this.todos.sort((a, b) => a.completed === b.completed ? 0 : a.completed ? -1 : b.completed ? 1 : 0) :
             this.todos.sort((a, b) => a.completed === b.completed ? 0 : a.completed ? 1 : b.completed ? -1 : 0);
        this.getPage(this.pagerNumber);
    }

    onModalClose(): void{
        this.editModalService.close('edit-modal');
    }

    onChangeData(event: boolean, todoItem: Todo): void{
        this.editedTodo = todoItem;
        if (event){
            this.editModalService.open('edit-modal');
            this.editText.nativeElement.value = todoItem.title;
            this.checkbox.nativeElement.checked = todoItem.completed;
        } else {
            this.todoListService.deleteTodo(todoItem.id).then(response => {
                if (response){
                    this.todos.splice(this.todos.findIndex(todo => todo.id === this.editedTodo.id), 1);
                    this.getPagerSize();
                }
            });
        }
    }

    onSaveTodo(): void{
        this.todoListService.patchTodo(this.editedTodo.id,
            {title: this.editText.nativeElement.value, completed: this.checkbox.nativeElement.checked}).then(response => {
            if (response){
                this.todos[this.todos.findIndex(todo => todo.id === this.editedTodo.id)] = response;
                this.getPage(this.pagerNumber);
                this.onModalClose();
            }
        });
    }
}
