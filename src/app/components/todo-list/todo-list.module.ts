import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EditModalModule } from '../edit-modal/edit-modal.module';
import { TodoItemComponent } from '../todo-item/todo-item.component';
import { TodoListComponent } from './todo-list.component';
import { TodolistService } from './todo-list.service';

@NgModule({
  declarations: [TodoListComponent, TodoItemComponent],
  imports: [CommonModule, EditModalModule],
  exports: [TodoListComponent, TodoItemComponent],
  providers: [TodolistService],
  bootstrap: []
})
export class TodoModule { }
