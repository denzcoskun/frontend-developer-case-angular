import { Address } from './address';
import { Company } from './company';

export class User {
    id: number;
    name: string;
    phone: string;
    username: string;
    website: string;
    email: string;
    company: Company;
    address: Address;

    constructor(id: number, name: string, phone: string, username: string, website: string, email: string, company: Company, address: Address){
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.username = username;
        this.website = website;
        this.email = email;
        this.company = company;
        this.address = address;
    }

}
