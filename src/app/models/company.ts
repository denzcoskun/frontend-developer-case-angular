export class Company {
    bs: string;
    catchPhrase: string;
    name: string;

    constructor(bs: string, catchPhrase: string, name: string){
        this.bs = bs;
        this.catchPhrase = catchPhrase;
        this.name = name;
    }

}
