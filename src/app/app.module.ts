import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { TodoModule } from './components/todo-list/todo-list.module';
import { EditModalModule } from './components/edit-modal/edit-modal.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TodoModule,
    HttpClientModule,
    EditModalModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
